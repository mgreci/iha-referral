var express = require("express");
var app = express();
const port = 3000;

app.set('view engine', 'pug')
app.use(express.static('public'))

app.listen(port, () => {
  console.log("Listening on port: " + port);
});

app.get("/", (req, res) => {
  res.render('index');
});
